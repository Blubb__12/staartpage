# Staartpage

Startpage for the HAAckwerk Aalen ![Sign Post](res/images/signpost-split.svg)

## Used Frameworks

- Bootstrap 5.2.0
- jQuery 3.6.0

## Image Sources

- Bootstrap Icons
- CCC Logo: Wikipedia

## Add sites

Sites are stored in "res/data/sites.json" there you can simply add something like

    "Example Page": {
        "url": "https://example.de/",
        "adminUrl": "https://example.de/admin/",
        "description": "Example Page"
    }

Arguments:
- The key is also the name of the page
- url should be obvious
- adminUrl is optional
- description is just a short one liner what the page is for

## Change Search URL

The URL for the Search Field is stored in "res/js/staartpage.js" as "searchUrl".

## Change Sites to external API

If you want to use an external API for the sites json just replace the "sitesUrl" in "res/js/staartpage.js" with the new one.
