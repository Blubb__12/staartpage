
var searchUrl = 'https://wiki.sfz-aalen.space/index.php?search=';
var sitesUrl = 'res/data/sites.json';

$(document).ready(function () {

    $.getJSON(sitesUrl, function(sites){

        var content = ''
        
        Object.keys(sites).forEach((k, i) => {
            content += `<div class="col">
                            <div class="card shadow-sm">
                                <div class="card-body">
                                    <h3 class="card-title">${k}</h3>
                                    <p class="card-text">${sites[k].description}</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <a type="button" target="_blank" href=${sites[k].url} class="btn btn-sm btn-outline-success">Visit</a>
                                            ${sites[k].adminUrl?`<a type="button" target="_blank" href=${sites[k].adminUrl} class="btn btn-sm btn-outline-danger">Admin</a>`:``}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`;
        })
        
        $('#elements').html(content);
        
    }).fail(function(){

        console.log("No connection to the sites.json.");
        
    });

    $('#searchButton').click(() => {

        let url = searchUrl + encodeURIComponent($('#searchField').val());

        console.log(url);

        window.open(url, '_blank');
    })
    
    
});